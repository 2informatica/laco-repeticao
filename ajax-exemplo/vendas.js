function exibirSites(urls){
    var container = document.
    getElementById('container');
    for(var pais of urls){
        var taga = document.
        createElement('a');
        taga.setAttribute('id', pais.id);
        taga.setAttribute('href', '#');
        var texto = document.
        createTextNode(pais.name);
        
        taga.appendChild(texto);
        container.appendChild(taga);
        container.appendChild(document.createElement('br'));
    }
}

var url = 'https://api.mercadolibre.com/sites';
var configuracoes = {
    method: 'get'
};

fetch(url, configuracoes).
then(function(resposta){
    return resposta.json();
}).then(function(resposta){
    exibirSites(resposta);
});