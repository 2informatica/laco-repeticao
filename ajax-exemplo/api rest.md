# API REST do mercado livre

## End points
* https://api.mercadolibre.com/sites (lista os sites existentes em cada país)
* https://api.mercadolibre.com/sites/MLB (informações do site do Brasil, tais como as categorias existentes)
* https://api.mercadolibre.com/categories/MLB5672 (informações de uma categoria)
* https://api.mercadolibre.com/sites/MLB/search#options (fornece uma lista de metodos de busca disponiveis)
* https://api.mercadolibre.com/sites/MLB/search?category=MLB5672 (lista produtos de uma categoria, o id da categoria é obtido a partir de /sites/MLB)
* https://api.mercadolibre.com/sites/MLB/search?q=tv (busca produtos através do nome)